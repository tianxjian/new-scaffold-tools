// 公共配置
// 引入webpack-merge合并插件
const {merge} = require('webpack-merge');
const common = require('./webpack.config.common.js');
const prod = require('./webpack.config.prod.js');
const dev = require('./webpack.config.dev.js');
module.exports = (env)=>{
    if(env.development){
        // merge 然后 放入要合并的 公用配置文件 和 开发模式文件就行了
        return merge(common,dev)
    }else if(env.production){
        return merge(common,prod)
    }else{
        return new error('没有设置环境')
    }
}