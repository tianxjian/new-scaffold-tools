// 开发配置
const path = require('path') // 引用path模块

module.exports = {
    devServer: {// 设置自动打包热更新
        // 可以指向我们当前服务的物理路径
        // static: path.resolve(__dirname,'../build/page1'),
        // compress 启动gzip压缩，提高传输效率
        compress: true,
        // 端口号
        port: 8888,
        // 自动打开浏览器
        open: true,
        // 开启代理，我们打包代码有时会含有特定接口的网络请求。我们现在本地是http://localhost:8888,跟我们的接口地址肯定是对不上的所以会发生跨域。这个时候就需要代理了
        proxy:{
            // 当我们输入接口中含有 /api 的时候，这个就会替换成设置好的地址
            '/api':'http://localhost:9999',
            secure:false, //如果是https请求，要配置这个参数
            // 路径重写，匹配到第一个这个字符/api，重写成 /
            pathRewrite:{'^/api':'/'}
        },
        // 当你用的是单页面的时候如vue，我们可以配置这个为true。也就是localhost:8888/aaa/bbb/ccc 不管你路由写的啥都会跳转到index.html首页中去的。
        historyApiFallback: true
    },
    mode: 'development' // development 开发模式
}