// 生产模式配置
const cssMinimizerWebpackPlugin=require('css-minimizer-webpack-plugin') // css代码压缩
const terserPlugin = require('terser-webpack-Plugin') // js代码压缩

module.exports = {
    // 使用optimization来引入压缩插件
    optimization: {
        minimizer: [
            new cssMinimizerWebpackPlugin(),
            // 代码压缩插件
            new terserPlugin()
        ],
    },
    performance: { // 不显示警告提示，如文件过大什么的
        hints: false
    },
    mode: 'production' // production 生产模式
}