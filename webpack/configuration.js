module.exports = {
	// 剔除第三方组件
	vendor:[
		'vue',
		'vue-router',
		'vuex'
	],
	// 配置多页面应用
	html:[
		// filename 多页面文件夹名称（必须放在src/page）
		// entrySite 入口配置js文件所在地址（相对于filename定义的文件夹路径）
		// htmlSite 页面html配置文件所在地址（相对于filename定义的文件夹路径）
		{filename:'page1',entrySite:'/main.js',htmlSite:'/index.html'},
		{filename:'page2',entrySite:'/main.js',htmlSite:'/index.html'}
	]
}