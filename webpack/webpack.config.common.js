// 开发和生产整体配置
const path = require('path') // 引用path模块
const {VueLoaderPlugin} = require('vue-loader')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const miniCssExtractPlugin = require('mini-css-extract-plugin')
const config = require('./configuration.js')

let entryConfig={ // 多页面js配置
    plugIn:{ // 将配置的插件分离打包到一个文件中
        import: config.vendor,
        filename: `plugIn/plugIn.[hash:5].js`,
    }
}
let htmlConfig=[] // 多页面html配置

for(let item of config.html){
    entryConfig[item.filename]={
        import:[`./src/page/${item.filename}${item.entrySite}`],
        dependOn: 'plugIn', // 插件部分不打包进此模块
        // 打包出来的文件地址和文件名
        filename: `${item.filename}/js/[name].[hash:10].js`,
    }
    htmlConfig.push(
        new HtmlWebpackPlugin({
            // html文件的地址
            template: `./src/page/${item.filename}${item.htmlSite}`,
            // 配置 引入的script标签所在位置，body就是放在<body>标签中
            inject: 'body',
            // 配置要引入的js文件，这里就是上面entry入口定义的文件名
            chunks: ['plugIn',`${item.filename}`],
            // 设置打包后的文件名和地址，不设置的话多个页面都用index名称会报错
            filename: `${item.filename}/index.html`,
        }),
    )
}

module.exports = {
    // 入口文件
    entry:entryConfig,
    // 打包后的出口文件
    output:{
        // publicPath: './',
        // 输出的路径
        path:path.resolve(__dirname,'../dist'),
        // 输出的文件名称
        filename:'plugIn/[hash:10].js',
        clean: true, // 设置每次打包先清除dist文件夹。
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    // 将less代码编译成css代码
                    'less-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    // 使用 miniCssExtractPlugin.loader，作用：提取js中的css文件拆分出来
                    // 就不在使用style-loader了
                    miniCssExtractPlugin.loader,
                    // css 将css文件变成commentJs模块加载到js中，里面的内容是样式字符串
                    'css-loader'
                ]
            },{
                test: /\.html$/,
                // 这个html的loader就是处理html中的img图片文件的（负责引入img，从而能被url-loader进行处理）
                loader: 'html-loader'
            },{ // 配置js代码兼容低版本浏览器
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },{
                test:/.(jpg|bmp|eps|gif|mif|miff|png|tif|tiff|svg|wmf|jpe|jpeg|dib|ico|tga|cut|pic)$/,
                loader: 'url-loader',
                options:{
                    // 图片大小小于12kb，就会base64处理
                    // 通常我们只会对8-12kb以下的图片进行base64处理
                    // 优点：减少请求数量（减轻服务器压力）
                    // 缺点：图片体积会更大（文件请求速度更慢）
                    limit: 1 * 1024,
                    name: '[hash:10].[ext]',
                    esModule: false,
                    // outputPath 就是将匹配的文件放到设置的文件夹里面
                    outputPath: 'assets/images'
                },
                // 配置了这个和esModule:false,html引入图片和css引入图片就可以显示了
                type:'javascript/auto'
            },{
				test:/\.(avi|asf|wmv|avs|flv|mov|3gp|mp4|mpg|mpeg|dat|ogm|vob|rm|rmvb|ts|tp|ifo|nsv)$/,
                loader: 'file-loader',
                options:{
                    name: '[hash:10].[ext]',
                    esModule: false,
                    outputPath: 'assets/video'
                }
			},
			{
				test:/\.(mp3|aac|wav|wma|cda|flac|m4a|mid|mka|mp2|mpa|mpc|ape|ofr|ogg|ra|wv|tta|ac3|dts)$/,
                loader: 'file-loader',
                options:{
                    name: '[hash:10].[ext]',
                    esModule: false,
                    outputPath: 'assets/audio'
                }
			},
			{
				test:/\.(eot|otf|fon|font|ttf|ttc|woff|woff2)$/,
                loader: 'file-loader',
                options:{
                    name: '[hash:10].[ext]',
                    esModule: false,
                    outputPath: 'assets/font'
                }
			},
			{
				test:/\.(exe|rar|zip|iso|doc|ppt|xls|xlsx|wps|txt|lrc|docx|pdf)$/,
                loader: 'file-loader',
                options:{
                    name: '[hash:10].[ext]',
                    esModule: false,
                    outputPath: 'assets/doc'
                }
			}
        ],
    },
    plugins:[
        new VueLoaderPlugin(),
        // 调用拆分css的插件
        new miniCssExtractPlugin({filename:'[name]/css/[hash:10].css'}),
        ...htmlConfig
    ]
}