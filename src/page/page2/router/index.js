import { createRouter, createWebHashHistory } from 'vue-router'
const Router = createRouter({
    history: createWebHashHistory(),
    routes: [{
        path: '/',
        redirect:"/login"
        },{
            path: '/home',
            name: 'home',
            component: ()=> import('../components/home.vue')
        },{
            path: '/login',
            name: 'login',
            component: ()=> import('../components/login.vue')
    }]
})
export default Router